1. CREATE DATABASE myshop;

2. -- Membuat tabel "users"
CREATE TABLE users (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255),
  password VARCHAR(255)
);

-- Membuat tabel "categories"
CREATE TABLE categories (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255)
);

-- Membuat tabel "items"
CREATE TABLE items (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  description VARCHAR(255),
  price INTEGER,
  stock INTEGER,
  category_id INTEGER,
  FOREIGN KEY (category_id) REFERENCES categories(id)
);